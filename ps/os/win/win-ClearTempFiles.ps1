# KITS  |  win-ClearTempFiles.ps1

# Add KITS as an event log source if needed
If (-Not([system.diagnostics.eventlog]::SourceExists(“KITS”))) {[system.diagnostics.EventLog]::CreateEventSource(“KITS”, “Application”)}

# Begin script
$TempFolders = @(“C:\Windows\Temp\*”, “C:\Windows\Prefetch\*”, “C:\Documents and Settings\*\Local Settings\temp\*”, “C:\Users\*\Appdata\Local\Temp\*”)
$SizeBefore = (Get-ChildItem –force $TempFolders -ErrorAction SilentlyContinue | measure Length -s)

Write-Host "Clearing temp files..."
Remove-Item $TempFolders -force -recurse

$SizeAfter = (Get-ChildItem –force $TempFolders -ErrorAction SilentlyContinue | measure Length -s)
$SizeChanged = "{0:N2} GB" -f ($SizeBefore - $SizeAfter) / 1Gb

Write-EventLog -LogName "Application" -Source "KITS" -EventID 4660 -EntryType Information -Message "Removed $SizeChanged Gb of temp files."
Write-Host "Removed $SizeChanged Gb of temp files."
